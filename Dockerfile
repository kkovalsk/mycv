FROM nginx:latest
COPY ./index.html /usr/share/nginx/html/index.html
COPY ./style.css /usr/share/nginx/html/style.css
COPY ./bg.jpg /usr/share/nginx/html/bg.jpg
COPY ./avatar.jpg /usr/share/nginx/html/avatar.jpg
COPY ./favicon.ico /usr/share/nginx/html/favicon.ico
